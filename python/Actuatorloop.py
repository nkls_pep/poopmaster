import mysql.connector
import RPi.GPIO as GPIO
from w1thermsensor import W1ThermSensor
import PID
import time

#DB variable
db=None

#Set Temp Sensor IDs
sensorIDsSorted=[]
sensorIDsSorted.append("0118769137ff")
sensorIDsSorted.append("0118769c21ff")
sensorIDsSorted.append("011876a715ff")
sensorIDsSorted.append("011876a0dcff")
sensorIDsSorted.append("0118769e01ff")
sensorIDsSorted.append("011876a498ff")
sensorIDsSorted.append("011876a449ff")
sensorIDsSorted.append("0118769c90ff")
sensorIDsSorted.append("011876bad9ff")
sensorIDsSorted.append("0118769d69ff")


#General GPIO setup
GPIO.setmode(GPIO.BCM)

#Setup Fan
GPIO.setup(14,GPIO.OUT)
FanPWMOut=GPIO.PWM(14,50)
FanPWMOut.start(0)

#Setup Valve
GPIO.setup(18,GPIO.OUT)

#Setup Servo
ServoPin=11
GPIO.setup(ServoPin,GPIO.OUT)
ServoPWMOut=GPIO.PWM(ServoPin,100)

#Setup PID
paramP=3
paramI=1
paramD=1
PIDReg=PID.PID(paramP,paramI,paramD)
PIDReg.setSampleTime(1)


def InitDB():
	global db
	db=mysql.connector.connect(
	host="192.168.1.201",
	user="scheisshaus",
	passwd="klo123",
	database="scheisshaus")
	return()

def SetFan(percentage:int):
	global FanPWMOut
	FanPWMOut.ChangeDutyCycle(percentage)
	return()

def SetValve(value:bool):
	if(value==True):
		GPIO.output(18,1)
	else:
		GPIO.output(18,0)
	return()

def SetServo(position:int):
	global ServoPWMOut
	value=(position/200)+.5
	value=value*10
	value=value+2.5
	ServoPWMOut.ChangeDutyCycle(value)
	return()

def GetFanSetValue():
	global db
	db.commit()
	dbcursor=db.cursor()
	sql="SELECT Wert FROM stellwerte WHERE Aktor='Luefter' ORDER BY Zeit DESC LIMIT 1;"
	dbcursor.execute(sql)
	dbresult=dbcursor.fetchone()
	value=float(dbresult[0])
	return(value)

def GetServoSetValue():
	global db
	db.commit()
	dbcursor=db.cursor()
	sql="SELECT Wert FROM stellwerte WHERE Aktor='MKlappe' ORDER BY Zeit DESC LIMIT 1;"
	dbcursor.execute(sql)
	dbresult=dbcursor.fetchone()
	value=int(dbresult[0])
	return(value)

def GetValveSetValue():
	global db
	db.commit()
	dbcursor=db.cursor()
	sql="SELECT Wert FROM stellwerte WHERE Aktor='Ventil' ORDER BY Zeit DESC LIMIT 1;"
	dbcursor.execute(sql)
	dbresult=dbcursor.fetchone()
	value=int(dbresult[0])
	return(value)

def GetTempSetValue():
	global db
	db.commit()
	dbcursor=db.cursor()
	sql="SELECT Wert FROM stellwerte WHERE Aktor='TSollwert' ORDER BY Zeit DESC LIMIT 1;"
	dbcursor.execute(sql)
	dbresult=dbcursor.fetchone()
	value=int(dbresult[0])
	return(value)


def GetTempIsValue(sensornumber:int):
	sensor=W1ThermSensor(W1ThermSensor.THERM_SENSOR_DS18B20,sensorIDsSorted[sensornumber])
	try:
		temp=sensor.get_temperature()
	except:
		temp=GetTempSetValue()
		print("Error reading Temp Is Value")
	return(temp)



def StoreHeatingPercentage(heatperc:int):
	global db
	db.commit()
	dbcursor=db.cursor()
	sql="INSERT INTO stellwerte (Aktor,Wert) VALUES (%s,%s);"
	val=("HeizProz",heatperc)
	dbcursor.execute(sql,val)
	db.commit()
	return()



def StoreSetTemp(settemp:float):
	global db
	db.commit()
	dbcursor=db.cursor()
	sql="INSERT INTO stellwerte (Aktor,Wert) VALUES (%s,%s);"
	val=("Sollwert",settemp)
	dbcursor.execute(sql,val)
	db.commit()
	return()

def StoreCurTemp(curtemp:float):
	global db
	db.commit()
	dbcursor=db.cursor()
	sql="INSERT INTO stellwerte (Aktor,Wert) VALUES(%s,%s);"
	val=("Istwert",curtemp)
	dbcursor.execute(sql,val)
	db.commit()
	return()

def ActuatorLoop(duration:int):
	while True:
		fan=GetFanSetValue()
		servo=GetServoSetValue()
		valve=GetValveSetValue()
		settemp=GetTempSetValue()
		curtemp=GetTempIsValue(0)

		StoreSetTemp(settemp)
		StoreCurTemp(curtemp)

		print("Lüfter       : ",str(fan)," %")
		print("Müffelklappe : ",str(servo), "%")
		print("Ventil       : ",str(valve))
		print("T Sollwert   : ",str(settemp), " °C")
		print("T Istwert    : ",str(curtemp), " °C")
		print("----------------------------------------")


		SetFan(fan)
		print("Lüfter gesetzt auf ",str(fan)," %")
		SetServo(servo)
		print("Müffelklappe gesetzt auf ",str(servo)," %")
		if(valve==0):
			SetValve(0)
			print("Ventil gesetzt auf AUS")
			time.sleep(duration)
		elif(valve==1):
			SetValve(1)
			print("Ventil gesetzt auf AN")
			time.sleep(duration)
		elif(valve==2):
			print("Ventil gesetzt auf AUTO")
			if(PIDReg.SetPoint!=settemp):
				PIDReg.SetPoint=settemp
			PIDReg.update(curtemp)
			PIDOutput=PIDReg.output
			print("PID Ausgabewert: ",str(PIDOutput))
			if(PIDOutput<0):
				PIDOutput=0
			if(PIDOutput>100):
				PIDOutput=100
			StoreHeatingPercentage(int(PIDOutput))
			ONDur=(PIDOutput/100)*duration
			OFFDur=duration-ONDur
			print("Ventil AN: ",str(ONDur), " s")
			SetValve(1)
			time.sleep(ONDur)
			print("Ventil AUS: ",str(OFFDur), " s")
			SetValve(0)
			time.sleep(OFFDur)

		print("===========================================")
InitDB()
ActuatorLoop(10)
