import os
import time
import keyboard
import sys
import tty
import termios
from w1thermsensor import W1ThermSensor
import Adafruit_DHT
import RPi.GPIO as GPIO
from hx711 import HX711
import mysql.connector

GPIO.setmode(GPIO.BCM)



db=None
def InitDB():
	global db
	db=mysql.connector.connect(
	host="localhost",
	user="scheisshaus",
	passwd="klo123",
	database="scheisshaus")



#T-Sensor-Gedöns
def Read_T_Values():
	sensors=[]

	sensorIDsSorted=[]

	sensorIDsSorted.append("0118769137ff")
	sensorIDsSorted.append("0118769c21ff")
	sensorIDsSorted.append("011876a715ff")
	sensorIDsSorted.append("011876a0dcff")
	sensorIDsSorted.append("0118769e01ff")
	sensorIDsSorted.append("011876a498ff")
	sensorIDsSorted.append("011876a449ff")
	sensorIDsSorted.append("0118769c90ff")
	sensorIDsSorted.append("011876bad9ff")
	sensorIDsSorted.append("0118769d69ff")

	if(sensors): del sensors[:]
	sensors=[]
	for sensor in W1ThermSensor.get_available_sensors():
		i=0
		while(i<10):
			#printat(0,29,i)
			if(sensor.id==sensorIDsSorted[i]):
				temp=sensor.get_temperature(W1ThermSensor.DEGREES_C)
				printat(7,5+i,"{:.1f}".format(temp)+" °C")
			i += 1

#T-RF-Gedöns
def Read_T_RF_Values():

	sensor1 = Adafruit_DHT.AM2302
	pin1=24
	sensor2=Adafruit_DHT.AM2302
	pin2=25
	sensor3=Adafruit_DHT.AM2302
	pin3=3

	hum1,temp1=Adafruit_DHT.read_retry(sensor1,pin1)
	printat(9,21,"{:.1f}".format(temp1)+" °C")
	printat(9,22,"{:.0f}".format(hum1)+" %")
	
	hum2,temp2=Adafruit_DHT.read_retry(sensor2,pin2)
	printat(9,24,"{:.1f}".format(temp2)+" °C")
	printat(9,25,"{:.0f}".format(hum2)+" %")
	
	hum3,temp3=Adafruit_DHT.read_retry(sensor3,pin3)
	printat(9,27,"{:.1f}".format(temp3)+" °C")
	printat(9,28,"{:.0f}".format(hum3)+" %")

	return

#Wägezellen-Gedöns:
def Read_Weight_Values():
	
	refUnitA1=-124
	refUnitA2=-124
	refUnitB1=-124
	refUnitB2=-124
	
	hxA1=HX711(17,27)
	hxA2=HX711(10,9)
	hxB1=HX711(22,23)
	hxB2=HX711(8,7)
	
	hxA1.set_reading_format("MSB", "MSB")
	hxA2.set_reading_format("MSB", "MSB")
	hxB1.set_reading_format("MSB", "MSB")
	hxB2.set_reading_format("MSB", "MSB")

	hxA1.set_reference_unit_A(1)
	hxA2.set_reference_unit_A(1)
	hxB1.set_reference_unit_A(1)
	hxB2.set_reference_unit_A(1)

	hxA1.set_gain(128)
	hxA2.set_gain(128)
	hxB1.set_gain(128)
	hxB2.set_gain(128)

	#hxA1.reset()
	#hxA2.reset()
	#hxB1.reset()
	#hxB2.reset()

	#hxA1.tare()
	#hxA2.tare()
	#hxB1.tare()
	#hxB2.tare()
	#print("Wägezellen tariert!")

	#hxA1.set_offset_A(-13973)
	#hxA2.set_offset_A(0)
	#hxB1.set_offset_A(0)
	#hxB2.set_offset_A(0)

	hxA1.set_reference_unit_A(refUnitA1)
	hxA2.set_reference_unit_A(refUnitA2)
	hxB1.set_reference_unit_A(refUnitB1)
	hxB2.set_reference_unit_A(refUnitB2)
	


	valA1=(hxA1.read_average(25)/refUnitA1)-30
	valA2=(hxA2.read_average(25)/refUnitA2)-2200
	printat(29,5,"         ")
	#printat(29,5,"{:.0f}".format(valA1+valA2)+" g")
	printat(29,5,"{:.0f}".format(valA1)+" "+"{:.0f}".format(valA2))
	valB1=(hxB1.read_average(25)/refUnitB1)-20
	valB2=(hxB2.read_average(25)/refUnitB2)-45
	printat(29,6,"         ")
	#printat(29,6,"{:.0f}".format(valB1+valB2)+" g")
	printat(29,6,"{:.0f}".format(valB1)+" "+"{:.0f}".format(valB2))

	hxA1.power_down()
	hxA2.power_down()
	hxA1.power_up()
	hxA2.power_up()
	return()



GPIO.setup(14,GPIO.OUT)
pwmOut=GPIO.PWM(14,50)
pwmOut.start(0)
def SetFan(percentage:int):
	#global db
	global pwmOut
	printat(29,8,"     ")
	pwmOut.ChangeDutyCycle(percentage)
	#dbcursor=db.cursor()
	#sql="INSERT INTO stellwerte (Aktor,Wert) VALUES ("Luefter",percentage);"
	#dbcursor.execute(sql)
	#printat(29,8,str(percentage)+" %")
	return()

GPIO.setup(18,GPIO.OUT)
def SetValve(value:bool):
	global db
	if(value==True):
		GPIO.output(18,1)
		#dbcursor=db.cursor()
		#sql="INSERT INTO stellwerte (Aktor,Wert) VALUES ("Ventil",1)"
		#dbcursor.execute(sql)
		#printat(20,14,"AUF")
	else:
		GPIO.output(18,0)
		#dbcursor=db.cursor()
		#sql="INSERT INTO stellwerte (Aktor,Wert) VALUES ("Ventil",0)"
		#dbcursor.execute(sql)
		#printat(20,14,"ZU ")
	return()

servoPin=11
GPIO.setup(servoPin,GPIO.OUT)
servoPWM=GPIO.PWM(servoPin,100)
servoPWM.start(7.5)
def SetServo(position:int):
	#global db
	global servoPWM
	value=(position/200)+.5
	value=value*10
	value=value+2.5
	printat(36,11,"      ")
	#dbcursor=db.cursor()
	#sql="INSERT INTO stellwerte (Aktor,Wert) VALUES ("MKlappe",position);"
	#dbcursor.execute(sql)
	servoPWM.ChangeDutyCycle(value)
	#printat(36,11,position)
	return()

def SetTSetValue(tvalue:int):
	pass

def printat(xpos:int,ypos:int,text:str):
	print("\033[",ypos,";",xpos,"H",text,sep='')
	return

def DrawLayout():
	os.system('clear')
	printat(30,0,"POOPMASTER UI 0.1")

	printat(0,3,"T-Sensoren")
	printat(0,4,"==========")
	printat(0,5,"T1  :")
	printat(0,6,"T2  :")
	printat(0,7,"T3  :")
	printat(0,8,"T4  :")
	printat(0,9,"T5  :")
	printat(0,10,"T6  :")
	printat(0,11,"T7  :")
	printat(0,12,"T8  :")
	printat(0,13,"T9  :")
	printat(0,14,"T10 :")

	printat(0,18,"T-RF-Sensoren:")
	printat(0,19,"==============")
	printat(0,20,"Einlass")
	printat(3,21,"T  :")
	printat(3,22,"RF :")
	printat(0,23,"Auslass A")
	printat(3,24,"T  :")
	printat(3,25,"RF :")
	printat(0,26,"Auslass B")
	printat(3,27,"T  :")
	printat(3,28,"RF :")

	printat(20,3,"Waagen:")
	printat(20,4,"============")
	printat(20,5,"links  :")
	printat(20,6,"rechts :")

	printat(20,8,"Lüfter : ")
	
	printat(20,9,"Müffelklappe :")

	printat(20,10,"Heizventil :")
	printat(20,11,"T-Sollwert :")
	


	printat(40,16,"Befehle:")
	printat(40,17,"========")
	printat(40,18,"T - T-Sensoren auslesen")
	printat(40,19,"F - T-RF-Sensoren auslesen")
	printat(40,20,"W - Wägezellen auslesen")
	printat(40,21,"1/2 - Lüfter A langsamer/schneller")
	printat(40,22,"Y/X - Müffelklappe links/rechts")
	printat(40,23,"H - Heizventil an/aus")
	printat(40,24,"3/4 - Temperatur runter/hoch")
	printat(40,25,"S - Stellwerte speichern und anwenden")
	printat(40,26,"Q - Quit")

	#printat(60,3,"       O")
	#printat(60,4,"   |  /|")
	#printat(60,5,"   |_(__\\")
	#printat(60,6,"    \  )\\")
	#printat(60,7,"     )( /_")
	#printat(60,8,"==============")

	printat(60,3,"     (   )")
	printat(60,4,"  (   ) (")
	printat(60,5,"   ) _   )")
	printat(60,6,"    ( \_")
	printat(60,7,"  _(_\ \)__")
	printat(60,8," (____\___))")

	return

def SetSaveActuators():
	global db
	dbcursor=db.cursor()
	SetFan(FanPercentage)
	sql="SELECT Wert FROM stellwerte WHERE Aktor='Luefter' ORDER BY Zeit ASC LIMIT 1;"
	dbcursor.execute(sql)
	dbresult=dbcursor.fetchone()
	if (dbresult!=FanPercentage):
		sql="INSERT INTO stellwerte (Aktor,Wert) VALUES (%s,%s);"
		val=("Luefter",FanPercentage)
		dbcursor.execute(sql,val)

	SetServo(ServoPosition)
	sql="SELECT Wert FROM stellwerte WHERE Aktor='MKlappe' ORDER BY Zeit ASC LIMIT 1;"
	dbcursor.execute(sql)
	dbresult=dbcursor.fetchone()
	if(dbresult!=ServoPosition):
		sql="INSERT INTO stellwerte (Aktor,Wert) VALUES (%s,%s);"
		val=("MKlappe",ServoPosition)
		dbcursor.execute(sql,val)

	SetValve(ValveStatus)
	sql="SELECT Wert FROM stellwerte WHERE Aktor='Ventil' ORDER BY Zeit ASC LIMIT 1;"
	dbcursor.execute(sql)
	dbresult=dbcursor.fetchone()
	if(dbresult!=ValveStatus):
		sql="INSERT INTO stellwerte (Aktor,Wert) VALUES (%s,%s);"
		val=("Ventil",ValveStatus)
		dbcursor.execute(sql,val)

	SetTSetValue(TSetValue)
	sql="SELECT Wert FROM stellwerte WHERE Aktor='TSollwert' ORDER BY Zeit ASC LIMIT 1;"
	dbcursor.execute(sql)
	dbresult=dbcursor.fetchone()
	if(dbresult!=TSetValue):
		sql="INSERT INTO stellwerte (Aktor,Wert) VALUES (%s,%s);"
		val=("TSollwert",TSetValue)
		dbcursor.execute(sql,val)

	db.commit()

def ReadActuatorValues():
	global db
	dbcursor=db.cursor()

	sql="SELECT Wert FROM stellwerte WHERE Aktor='Luefter' ORDER BY Zeit DESC LIMIT 1;"
	dbcursor.execute(sql)
	dbresult=dbcursor.fetchone()
	FanPercentage=float(dbresult[0])
	SetFan(FanPercentage)
	printat(35,8,str(FanPercentage)+" %")

	sql="SELECT Wert FROM stellwerte WHERE Aktor='MKlappe' ORDER BY Zeit DESC LIMIT 1;"
	dbcursor.execute(sql)
	dbresult=dbcursor.fetchone()
	ServoPosition=int(dbresult[0])
	SetServo(int(ServoPosition))
	printat(35,9,str(ServoPosition))

	sql="SELECT Wert FROM stellwerte WHERE Aktor='Ventil' ORDER BY Zeit DESC LIMIT 1;"
	dbcursor.execute(sql)
	dbresult=dbcursor.fetchone()
	ValveStatus=dbresult[0]
	SetValve(ValveStatus)
	if(ValveStatus==1):
		printat(35,10,"AUF")
	else:
		printat(35,10,"ZU ")

	sql="SELECT Wert FROM stellwerte WHERE Aktor='TSollwert' ORDER BY Zeit DESC LIMIT 1;"
	dbcursor.execute(sql)
	dbresult=dbcursor.fetchone()
	TSetValue=dbresult[0]
	
	printat(35,11,str(TSetValue)+ " °C")



def getch():
	fd = sys.stdin.fileno()
	old_settings = termios.tcgetattr(fd)
	try:
		tty.setraw(sys.stdin.fileno())
		ch = sys.stdin.read(1)

	finally:
		termios.tcsetattr(fd, termios.TCSADRAIN, old_settings)
	return ch



DrawLayout()
InitDB()
ReadActuatorValues()

button_delay = 0.2
FanPercentage =0
ValveStatus=False
ServoPosition=0
TSetValue=30

while True:
	char = getch()
	printat(40,29,"                                        ")
	if (char == "t"):
		printat(40,29,"T-Fühler auslesen")
		Read_T_Values()
		printat(40,29,"                 ")
		time.sleep(button_delay)
	elif (char == "f"):
		printat(40,29,"T-RF-Fühler auslesen")
		Read_T_RF_Values()
		printat(40,29,"                    ")
		time.sleep(button_delay)
	elif (char == "w"):
		printat(40,29,"Wägezellen auslesen")
		Read_Weight_Values()
		printat(40,29,"                   ")
		time.sleep(button_delay)
	
	elif (char == "1"):
		printat(40,29,"Lüfter langsamer")
		if(FanPercentage<10):
			FanPercentage=0
		else:
			FanPercentage -= 10
		#SetFan(FanPercentage)
		printat(29,8,str(FanPercentage)+" %")
		time.sleep(button_delay)
		printat(40,29,"                ")
	elif (char == "2"):
		printat(40,29,"Lüfter schneller")
		if(FanPercentage>90):
			FanPercentage=100
		else:
			FanPercentage += 10
		#SetFan(FanPercentage)
		printat(29,8,str(FanPercentage)+" %")
		time.sleep(button_delay)
		printat(40,29,"                ")

	elif (char=="3"):
		printat(40,29,"T-Sollwert runter")
		if(TSetValue<=20):
			TSetValue=20
		else:
			TSetValue-=1
		printat(35,11,str(TSetValue)+" °C")
		time.sleep(button_delay)
		printat(40,29,"                 ")

	elif (char=="4"):
		printat(40,29,"T-Sollwert rauf")
		if(TSetValue>=90):
			TSetValue=90
		else:
			TSetValue+=1
		printat(35,11,str(TSetValue)+" °C ")
		time.sleep(button_delay)
		printat(40,29,"                 ")

	elif (char == "y"):
		printat(40,29,"Klappe links")
		if(ServoPosition<(-90)):
			ServoPosition=-100
		else:
			ServoPosition -= 10
		#setServo(ServoPosition)
		time.sleep(button_delay)
		printat(40,29,"            ")
		printat(36,11,ServoPosition)
	elif (char == "x"):
		printat(40,29,"Klappe rechts")
		if(ServoPosition>90):
			ServoPosition=100
		else:
			ServoPosition+=10
		setServo(ServoPosition)
		time.sleep(button_delay)
		printat(40,29,"             ")
	elif (char == "h"):
		if(ValveStatus==False):
			printat(40,29,"Ventil auf")
			ValveStatus=True
			#SetValve(ValveStatus)
			printat(20,14,"AUF")
		else:
			printat(40,29,"Ventil zu")
			ValveStatus=False
			#SetValve(ValveStatus)
			printat(20,14,"ZU ")
		time.sleep(button_delay)
		printat(40,29,"          ")
		
	elif (char == "q"):
		printat(40,29,"Quit")
		exit(0)

	elif (char == "s"):
		SetSaveActuators()
		printat(40,29,"Stellwerte angewandt und gespeichert")

	elif (char == "i"):
		printat(40,29,"Das Imperium schlägt zurück!")
	elif (char == "c"):
		print(40,29,"!!!!!CYBER-ALARM!!!1!!")

	elif (char == "r"):
		DrawLayout()
		time.sleep(button_delay)
