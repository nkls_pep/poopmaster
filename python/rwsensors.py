import mysql.connector
from w1thermsensor import W1ThermSensor
import Adafruit_DHT
import RPi.GPIO as GPIO
from hx711 import HX711

db=None

def Init_DB():
	global db
	db = mysql.connector.connect(
	host="192.168.1.201",
#	host="localhost",
	user="scheisshaus",
	passwd="klo123",
	database="scheisshaus")
	print("DB verbunden")

def Get_T_Values():
	sensors=[]

	sensorIDsSorted=[] 

	sensorIDsSorted.append("0118769137ff") 
	sensorIDsSorted.append("0118769c21ff") 
	sensorIDsSorted.append("011876a715ff") 
	sensorIDsSorted.append("011876a0dcff") 
	sensorIDsSorted.append("0118769e01ff")
	sensorIDsSorted.append("011876a498ff")
	sensorIDsSorted.append("011876a449ff")
	sensorIDsSorted.append("0118769c90ff")
	sensorIDsSorted.append("011876bad9ff")
	sensorIDsSorted.append("0118769d69ff")

	if(sensors): del sensors[:]
	sensors=[]
	sensorsSorted=[]
	for sensor in W1ThermSensor.get_available_sensors():
		i=0
		while(i<10):
			if(sensor.id==sensorIDsSorted[i]):
				try:
					temp=sensor.get_temperature(W1ThermSensor.DEGREES_C)
				except:
					temp=0
				sensorsSorted.append("{:.1f}".format(temp))
			i += 1
	print("T-Sensoren gelesen")
	return(sensorsSorted)

def Get_T_RF_Values():
	valueArray=[]
	sensor1 = Adafruit_DHT.AM2302
	pin1=24
	sensor2=Adafruit_DHT.AM2302
	pin2=25
	sensor3=Adafruit_DHT.AM2302
	pin3=3

	try:
		hum1,temp1=Adafruit_DHT.read_retry(sensor1,pin1)
	except:
		hum1=0
		temp1=0
	
	try:
		hum2,temp2=Adafruit_DHT.read_retry(sensor2,pin2)
	except:
		hum2=0
		temp2=0

	try:
		hum3,temp3=Adafruit_DHT.read_retry(sensor3,pin3)
	except:
		hum3=0
		temp3=0

	valueArray.append("{:.1f}".format(temp1))
	valueArray.append("{:.1f}".format(temp2))
	valueArray.append("{:.1f}".format(temp3))
	valueArray.append("{:.0f}".format(hum1))
	valueArray.append("{:.0f}".format(hum2))
	valueArray.append("{:.0f}".format(hum3))
	
	print("T-RF-Sensoren gelesen")
	return(valueArray)

def Get_Weight_Values():

	refUnitA1=-124
	refUnitA2=-124
	refUnitB1=-124
	refUnitB2=-124

	hxA1=HX711(17,27)
	hxA2=HX711(10,9)
	hxB1=HX711(22,23)
	hxB2=HX711(8,7)

	hxA1.set_reading_format("MSB", "MSB")
	hxA2.set_reading_format("MSB", "MSB")
	hxB1.set_reading_format("MSB", "MSB")
	hxB2.set_reading_format("MSB", "MSB")

	hxA1.set_reference_unit_A(1)
	hxA2.set_reference_unit_A(1)
	hxB1.set_reference_unit_A(1)
	hxB2.set_reference_unit_A(1)

	hxA1.set_gain(128)
	hxA2.set_gain(128)
	hxB1.set_gain(128)
	hxB2.set_gain(128)

	hxA1.set_reference_unit_A(refUnitA1)
	hxA2.set_reference_unit_A(refUnitA2)
	hxB1.set_reference_unit_A(refUnitB1)
	hxB2.set_reference_unit_A(refUnitB2)

	try:
		valA1=(hxA1.read_average(1)/refUnitA1)-30
	except:
		valA1=0

	try:
		valA2=(hxA2.read_average(1)/refUnitA2)-2200
	except:
		valA2=0

	try:
		valB1=(hxB1.read_average(25)/refUnitB1)-20
	except:
		valB1=0

	try:
		valB2=(hxB1.read_average(25)/refUnitB2)-45
	except:
		valB2=0

	hxA1.power_down()
	hxA2.power_down()
	hxA1.power_up()
	hxA2.power_up()

	valueArray=[]
	valueArray.append("{:.0f}".format(valA1))
	valueArray.append("{:.0f}".format(valA2))
	valueArray.append("{:.0f}".format(valB1))
	valueArray.append("{:.0f}".format(valB2))
	
	for w in valueArray:
		print(str(w))
	print("Wägezellen gelesen")
	return(valueArray)

def Get_Fan_Value():
	global db
	cursor = db.cursor()
	sql="SELECT Wert FROM stellwerte WHERE Aktor='Luefter' ORDER BY Zeit DESC LIMIT 1;"
	cursor.execute(sql)
	result = cursor.fetchone()
	value="{:.0f}".format(result[0])
	print("Lüfter-Stellwert gelesen")
	return(value)

def Get_Flap_Value():
	global db
	cursor=db.cursor()
	sql="SELECT Wert FROM stellwerte WHERE Aktor='MKlappe' ORDER BY Zeit DESC LIMIT 1;"
	cursor.execute(sql)
	result=cursor.fetchone()
	value="{:.0f}".format(result[0])
	print("Müffelklappen-Stellwert gelesen")
	return(value)

def Get_Valve_Value():
	global db
	cursor=db.cursor()
	sql="SELECT Wert FROM stellwerte WHERE Aktor='Ventil' ORDER BY Zeit DESC LIMIT 1;"
	cursor.execute(sql)
	result=cursor.fetchone()
	value="{:.0f}".format(result[0])
	print("Ventil-Stellwert gelesen")
	return(value)

def Get_PIDout_value():
	global db
	cursor=db.cursor()
	sql="SELECT Wert FROM stellwerte WHERE Aktor='HeizProz' ORDER BY Zeit DESC LIMIT 1;"
	cursor.execute(sql)
	result=cursor.fetchone()
	value="{:.0f}".format(result[0])
	print("PID-Output fuer Heizer gelesen")
	return(value)

def Get_SetValue():
	global db
	cursor=db.cursor()
	sql="SELECT Wert FROM stellwerte WHERE Aktor='Sollwert' ORDER BY Zeit DESC LIMIT 1;"
	cursor.execute(sql)
	result=cursor.fetchone()
	value="{:.0f}".format(result[0])
	print("T-Sollwert gelesen")
	return(value)

def Get_CurValue():
	global db
	cursor=db.cursor()
	sql="SELECT Wert FROM stellwerte WHERE Aktor='Istwert' ORDER BY Zeit DESC LIMIT 1;"
	cursor.execute(sql)
	result=cursor.fetchone()
	value="{:.0f}".format(result[0])
	print("T-Istwert geelesen")
	return(value)



def Write_Values():
	global db

	t=Get_T_Values()
	t_rf=Get_T_RF_Values()
	w=Get_Weight_Values()
	fa=Get_Fan_Value()
	fl=Get_Flap_Value()
	v=Get_Valve_Value()
	pid=Get_PIDout_value()
	setval=Get_SetValue()
	curval=Get_CurValue()
	sql="INSERT INTO daten \
		(T1,T2,T3,T4,T5,T6,T7,T8,T9,T10, \
		T_Einlass,T_Auslass_A,T_Auslass_B, \
		RF_Einlass,RF_Auslass_A,RF_Auslass_B, \
		m_A1,m_A2,m_B1,m_B2,Luefter,MKlappe,Ventil, \
		HeizProz,Sollwert,Istwert) VALUES ("
	for temp in t:
		sql=sql+temp+","
	for trf in t_rf:
		sql=sql+trf+","
	for weight in w:
		sql=sql+weight+","
	sql=sql+fa+","
	sql=sql+fl+","
	sql=sql+v+","
	sql=sql+pid+","
	sql=sql+setval+","
	sql=sql+curval+");"
	#print(sql)

	cursor=db.cursor()
	cursor.execute(sql)
	db.commit()
	print("Datensatz geschrieben")

Init_DB()
Write_Values()

#sql = "INSERT INTO daten (zeit, temperatur) VALUES (%s, %s)"
#val = ("2019-08-11 08:00:00", "28")
#mycursor.execute(sql, val)

#mydb.commit()

#print(mycursor.rowcount, "record inserted.")
