import time
import RPi.GPIO as GPIO

GPIO.setmode(GPIO.BCM)
GPIO.setup(14,GPIO.OUT)

pwmOut=GPIO.PWM(14,300)
pwmOut.start(0)

dutyCycle=0

while(1):
	time.sleep(3)
	dutyCycle=dutyCycle+10

	if(dutyCycle>100):
		dutyCycle=0
		time.sleep(3)
		pwmOut.ChangeDutyCycle(dutyCycle)
		time.sleep(3)
	pwmOut.ChangeDutyCycle(dutyCycle)
	print("duty cycle: ",dutyCycle)
