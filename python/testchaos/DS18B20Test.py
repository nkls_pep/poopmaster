import time
from w1thermsensor import W1ThermSensor

sensors=[]

def detectSensors():
	global sensors
	if(sensors): del sensors[:]
	sensors=[]
	for sensor in W1ThermSensor.get_available_sensors():
		sensors.insert(0,sensor)
		print("Sensor ID=%s"%(sensor.id))

def readTemps():
	global sensors
	for sensor in sensors:
		temp=sensor.get_temperature(W1ThermSensor.DEGREES_C)
		print(sensor.id,"T: ",temp)


detectSensors()

while(1):
	readTemps()
	print("Ende")
	time.sleep(5)
	
