import time
import RPi.GPIO as GPIO

Nc = 261
Nd = 294
Ne = 329
Nf = 349
Ng = 391
NgS = 415
Na = 440
NaS = 455
Nb = 466
NcH = 523
NcSH = 554
NdH = 587
NdSH = 622
NeH = 659
NfH = 698
NfSH = 740
NgH = 784
NgSH = 830
NaH = 880


GPIO.setmode(GPIO.BCM)
GPIO.setup(14,GPIO.OUT)
pwmOut=GPIO.PWM(14,200)
pwmOut.start(0)

def PlayNote(frequ,dur):
	pwmOut.ChangeFrequency(frequ)
	pwmOut.ChangeDutyCycle(60)
	time.sleep(dur/1000)
	return

def firstSection():
	PlayNote(Na,500)
	PlayNote(Na,500)    
	PlayNote(Na,500)
	PlayNote(Nf,350)
	PlayNote(NcH,150)  
	PlayNote(Na,500)
	PlayNote(Nf,350)
	PlayNote(NcH,150)
	PlayNote(Na,650)
	time.sleep(.5)
	PlayNote(NeH,500)
	PlayNote(NeH,500)
	PlayNote(NeH,500)  
	PlayNote(NfH,350)
	PlayNote(NcH, 150)
	PlayNote(NgS,500)
	PlayNote(Nf,350)
	PlayNote(NcH, 150)
	PlayNote(Na,650)
	time.sleep(.5)
	return 0

def secondSection():
	PlayNote(NaH,500)
	PlayNote(Na,300)
	PlayNote(Na,150)
	PlayNote(NaH,500)
	PlayNote(NgSH,325)
	PlayNote(NgH,175)
	PlayNote(NfSH,125)
	PlayNote(NfH,125)
	PlayNote(NfSH,250)
	time.sleep(.3)
	PlayNote(NaS,250)
	PlayNote(NdSH,500)
	PlayNote(NdH,325)
	PlayNote(NcSH,175)
	PlayNote(NcH,125)
	PlayNote(Nb,125)
	PlayNote(NcH,250)
	time.sleep(.4)
	return 0

while(1):
	firstSection()
	print("first")
	secondSection()
	print("second")
