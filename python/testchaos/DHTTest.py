import time
import Adafruit_DHT

sensor1 = Adafruit_DHT.AM2302
pin1=24
sensor2=Adafruit_DHT.AM2302
pin2=25
sensor3=Adafruit_DHT.AM2302
pin3=3
while(1):
	hum1,temp1=Adafruit_DHT.read_retry(sensor1,pin1)
	hum2,temp2=Adafruit_DHT.read_retry(sensor2,pin2)
	hum3,temp3=Adafruit_DHT.read_retry(sensor3,pin3)
	if hum1 is not None and temp1 is not None:
		print('Temp1={0:0.1f}*C  Hum1={1:0.1f}%'.format(temp1, hum1))
	else:
		print('Failed to get reading Sensor 1. Try again!')

	if hum2 is not None and temp2 is not None:
		print('Temp2={0:0.1f}*C  Hum2={1:0.1f}%'.format(temp2,hum2))
	else:
		print('Failed to get reading Sensor 2. Try again!')
	if hum3 is not None and temp3 is not None:
		print('Temp3={0:0.1f}*C  Hum3={1:0.1f}%'.format(temp3,hum3))
	else:
		print('Failed to get reading Sensor 3. Try again!')
	print('Ende')
	time.sleep(3)
	
