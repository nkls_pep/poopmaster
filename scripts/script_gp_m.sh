#!/bin/sh

#alle Daten lesen
#mysql -u scheisshaus --password=klo123 -D scheisshaus --skip-column-names -e "SELECT Zeit,m_A1,m_A2,m_B1,m_B2 FROM daten order by Zeit asc;" | sed 's/\t/;/g;s/\n//g' > /opt/poopmaster/data/m_gp.csv

#letzte Stunde
mysql -h 192.168.1.201 -u scheisshaus --password=klo123 -D scheisshaus --skip-column-names -e \
"SELECT Zeit,m_A1,m_A2,m_B1,m_B2 FROM daten WHERE Zeit >= DATE_SUB(NOW(),INTERVAL 1 HOUR) order by Zeit asc;" | \
sed 's/\t/;/g;s/\n//g' > /opt/poopmaster/data/m_gp_lstunde.csv

#heute
mysql -h 192.168.1.201 -u scheisshaus --password=klo123 -D scheisshaus --skip-column-names -e \
"SELECT Zeit,m_A1,m_A2,m_B1,m_B2 FROM daten WHERE DATE(Zeit)=CURDATE() order by Zeit asc;" | \
sed 's/\t/;/g;s/\n//g' > /opt/poopmaster/data/m_gp_heute.csv

#gestern
mysql -h 192.168.1.201 -u scheisshaus --password=klo123 -D scheisshaus --skip-column-names -e \
"SELECT Zeit,m_A1,m_A2,m_B1,m_B2 FROM daten WHERE DATE(Zeit)=SUBDATE(CURDATE(),1) order by Zeit asc;" | \
sed 's/\t/;/g;s/\n//g' > /opt/poopmaster/data/m_gp_gestern.csv

#diese Woche
mysql -h 192.168.1.201 -u scheisshaus --password=klo123 -D scheisshaus --skip-column-names -e \
"SELECT Zeit,m_A1,m_A2,m_B1,m_B2 FROM daten WHERE YEARWEEK(Zeit,1)=YEARWEEK(NOW(),1) order by Zeit asc;" | \
sed 's/\t/;/g;s/\n//g' > /opt/poopmaster/data/m_gp_dwoche.csv

#letzte Woche
mysql -h 192.168.1.201 -u scheisshaus --password=klo123 -D scheisshaus --skip-column-names -e \
"SELECT Zeit,m_A1,m_A2,m_B1,m_B2 FROM daten WHERE YEARWEEK(Zeit,1) = YEARWEEK(NOW() - INTERVAL 1 WEEK,1) order by Zeit asc;" | \
sed 's/\t/;/g;s/\n//g' > /opt/poopmaster/data/m_gp_lwoche.csv

#dieser Monat
mysql -h 192.168.1.201 -u scheisshaus --password=klo123 -D scheisshaus --skip-column-names -e \
"SELECT Zeit,m_A1,m_A2,m_B1,m_B2 FROM daten WHERE YEAR(Zeit) = YEAR(CURRENT_DATE()) \
AND MONTH(Zeit) = MONTH(CURRENT_DATE()) order by Zeit asc;" | \
sed 's/\t/;/g;s/\n//g' > /opt/poopmaster/data/m_gp_dmonat.csv

#letzter Monat
mysql -h 192.168.1.201 -u scheisshaus --password=klo123 -D scheisshaus --skip-column-names -e \
"SELECT Zeit,m_A1,m_A2,m_B1,m_B2 FROM daten WHERE YEAR(Zeit) = YEAR(CURRENT_DATE - INTERVAL 1 MONTH) \
AND MONTH(Zeit) = MONTH(CURRENT_DATE - INTERVAL 1 MONTH) order by Zeit asc;" | \
sed 's/\t/;/g;s/\n//g' > /opt/poopmaster/data/m_gp_lmonat.csv

TITLE="Waagen"
/usr/bin/gnuplot << EOF
set datafile separator ';'
set title "$TITLE"
set grid
set xlabel "Zeit"
set autoscale x
set yrange [-500:10000]
set ylabel "Gewicht (g)"
set xdata time
set timefmt "%Y-%m-%d %H:%M:%S"
set format x "%d.%m\n%R" 
set key outside right center box title ""
set terminal png large size 1920,1080

set output "/var/www/html/images/plot_m_lstunde.png"
plot "/opt/poopmaster/data/m_gp_lstunde.csv" using 1:2 title "Zelle A1" with lines lw 3 \
,"" using 1:3 title "Zelle A2" with lines lw 3  \
,"" using 1:4 title "Zelle B1" with lines lw 3  \
,"" using 1:5 title "Zelle B2" with lines lw 3  \
#,"" using 1:(column(2)+column(3)) title "gesamt A" with lines \
#,"" using 1:(column(4)+column(5)) title "gesamt B" with lines

set output "/var/www/html/images/plot_m_heute.png"
plot "/opt/poopmaster/data/m_gp_heute.csv" using 1:2 title "Zelle A1" with lines lw 3  \
,"" using 1:3 title "Zelle A2" with lines lw 3  \
,"" using 1:4 title "Zelle B1" with lines lw 3  \
,"" using 1:5 title "Zelle B2" with lines lw 3  \
#,"" using 1:(column(2)+column(3)) title "gesamt A" with lines \
#,"" using 1:(column(4)+column(5)) title "gesamt B" with lines

set output "/var/www/html/images/plot_m_gestern.png"
plot "/opt/poopmaster/data/m_gp_gestern.csv" using 1:2 title "Zelle A1" with lines lw 3  \
,"" using 1:3 title "Zelle A2" with lines lw 3  \
,"" using 1:4 title "Zelle B1" with lines lw 3  \
,"" using 1:5 title "Zelle B2" with lines lw 3  \
#,"" using 1:(column(2)+column(3)) title "gesamt A" with lines \
#,"" using 1:(column(4)+column(5)) title "gesamt B" with lines

set output "/var/www/html/images/plot_m_dwoche.png"
plot "/opt/poopmaster/data/m_gp_dwoche.csv" using 1:2 title "Zelle A1" with lines lw 3  \
,"" using 1:3 title "Zelle A2" with lines lw 3  \
,"" using 1:4 title "Zelle B1" with lines lw 3  \
,"" using 1:5 title "Zelle B2" with lines lw 3  \
#,"" using 1:(column(2)+column(3)) title "gesamt A" with lines \
#,"" using 1:(column(4)+column(5)) title "gesamt B" with lines

set output "/var/www/html/images/plot_m_lwoche.png"
plot "/opt/poopmaster/data/m_gp_lwoche.csv" using 1:2 title "Zelle A1" with lines lw 3  \
,"" using 1:3 title "Zelle A2" with lines lw 3  \
,"" using 1:4 title "Zelle B1" with lines lw 3  \
,"" using 1:5 title "Zelle B2" with lines lw 3  \
#,"" using 1:(column(2)+column(3)) title "gesamt A" with lines \
#,"" using 1:(column(4)+column(5)) title "gesamt B" with lines

set output "/var/www/html/images/plot_m_dmonat.png"
plot "/opt/poopmaster/data/m_gp_dmonat.csv" using 1:2 title "Zelle A1" with lines lw 3 \
,"" using 1:3 title "Zelle A2" with lines lw 3  \
,"" using 1:4 title "Zelle B1" with lines lw 3  \
,"" using 1:5 title "Zelle B2" with lines lw 3  \
#,"" using 1:(column(2)+column(3)) title "gesamt A" with lines \
#,"" using 1:(column(4)+column(5)) title "gesamt B" with lines

set output "/var/www/html/images/plot_m_lmonat.png"
plot "/opt/poopmaster/data/m_gp_lmonat.csv" using 1:2 title "Zelle A1" with lines lw 3  \
,"" using 1:3 title "Zelle A2" with lines lw 3  \
,"" using 1:4 title "Zelle B1" with lines lw 3  \
,"" using 1:5 title "Zelle B2" with lines lw 3  \
#,"" using 1:(column(2)+column(3)) title "gesamt A" with lines \
#,"" using 1:(column(4)+column(5)) title "gesamt B" with lines
EOF

VAR1="/opt/poopmaster/data/wochengrafiken/plot_m_KW"
VAR2=`date +%V`
VAR3=".png"
VAR4="$VAR1$VAR2$VAR3"
cp /var/www/html/images/plot_m_lwoche.png $VAR4
