#!/bin/sh

#alle Daten lesen
#mysql -u scheisshaus --password=klo123 -D scheisshaus --skip-column-names -e \
#"SELECT Zeit,Luefter,MKlappe,Ventil,HeizProz FROM daten order by Zeit asc;" | \
#sed 's/\t/;/g;s/\n//g' > /opt/poopmaster/data/LMV_gp.csv

#letzte Stunde
mysql -h 192.168.1.201 -u scheisshaus --password=klo123 -D scheisshaus --skip-column-names -e \
"SELECT Zeit,Luefter,MKlappe,Ventil,HeizProz FROM daten WHERE Zeit >= DATE_SUB(NOW(),INTERVAL 1 HOUR) order by Zeit asc;" | \
sed 's/\t/;/g;s/\n//g' > /opt/poopmaster/data/LMV_gp_lstunde.csv

#heute
mysql -h 192.168.1.201 -u scheisshaus --password=klo123 -D scheisshaus --skip-column-names -e \
"SELECT Zeit,Luefter,MKlappe,Ventil,HeizProz FROM daten WHERE DATE(Zeit)=CURDATE() order by Zeit asc;" | \
sed 's/\t/;/g;s/\n//g' > /opt/poopmaster/data/LMV_gp_heute.csv

#gestern
mysql -h 192.168.1.201 -u scheisshaus --password=klo123 -D scheisshaus --skip-column-names -e \
"SELECT Zeit,Luefter,MKlappe,Ventil,HeizProz FROM daten WHERE DATE(Zeit)=SUBDATE(CURDATE(),1) order by Zeit asc;" | \
sed 's/\t/;/g;s/\n//g' > /opt/poopmaster/data/LMV_gp_gestern.csv

#diese Woche
mysql -h 192.168.1.201 -u scheisshaus --password=klo123 -D scheisshaus --skip-column-names -e \
"SELECT Zeit,Luefter,MKlappe,Ventil,HeizProz FROM daten WHERE YEARWEEK(Zeit,1)=YEARWEEK(NOW(),1) order by Zeit asc;" | \
sed 's/\t/;/g;s/\n//g' > /opt/poopmaster/data/LMV_gp_dwoche.csv

#letzte Woche
mysql -h 192.168.1.201 -u scheisshaus --password=klo123 -D scheisshaus --skip-column-names -e \
"SELECT Zeit,Luefter,MKlappe,Ventil,HeizProz FROM daten WHERE YEARWEEK(Zeit,1) = YEARWEEK(NOW() - INTERVAL 1 WEEK,1) order by Zeit asc;" | \
sed 's/\t/;/g;s/\n//g' > /opt/poopmaster/data/LMV_gp_lwoche.csv

#dieser Monat
mysql -h 192.168.1.201 -u scheisshaus --password=klo123 -D scheisshaus --skip-column-names -e \
"SELECT Zeit,Luefter,MKlappe,Ventil,HeizProz FROM daten WHERE YEAR(Zeit) = YEAR(CURRENT_DATE()) \
AND MONTH(Zeit) = MONTH(CURRENT_DATE()) order by Zeit asc;" | \
sed 's/\t/;/g;s/\n//g' > /opt/poopmaster/data/LMV_gp_dmonat.csv

#letzter Monat
mysql -h 192.168.1.201 -u scheisshaus --password=klo123 -D scheisshaus --skip-column-names -e \
"SELECT Zeit,Luefter,MKlappe,Ventil,HeizProz FROM daten WHERE YEAR(Zeit) = YEAR(CURRENT_DATE - INTERVAL 1 MONTH) \
AND MONTH(Zeit) = MONTH(CURRENT_DATE - INTERVAL 1 MONTH) order by Zeit asc;" | \
sed 's/\t/;/g;s/\n//g' > /opt/poopmaster/data/LMV_gp_lmonat.csv


TITLE="Lüfter, Müffelklappe, Heizventil, Stellwert"
/usr/bin/gnuplot << EOF
#set datafile missing "NULL"
set datafile separator ';'
set title "$TITLE"
set grid
set xlabel "Zeit"
set autoscale x
set yrange [-110:110]
set ylabel "%"
set xdata time
set timefmt "%Y-%m-%d %H:%M:%S"
set format x "%d.%m\n%R" 
set key outside right center box title ""
set terminal png large size 1920,1080


set output "/var/www/html/images/plot_LMV_lstunde.png"
plot "/opt/poopmaster/data/LMV_gp_lstunde.csv" using 1:2 title "Lüfter" with lines lw 3 \
,"" using 1:3 title "Müffelklappe" with lines lw 3 \
,"" using 1:(column(4)*100) title "Ventil" with lines lw 3  \
,"" using 1:5 title "HZG Stellwert" with lines lw 3  \

set output "/var/www/html/images/plot_LMV_heute.png"
plot "/opt/poopmaster/data/LMV_gp_heute.csv" using 1:2 title "Lüfter" with lines lw 3  \
,"" using 1:3 title "Müffelklappe" with lines lw 3  \
,"" using 1:(column(4)*100) title "Ventil" with lines lw 3  \
,"" using 1:5 title "HZG Stellwert" with lines lw 3  \

set output "/var/www/html/images/plot_LMV_gestern.png"
plot "/opt/poopmaster/data/LMV_gp_gestern.csv" using 1:2 title "Lüfter" with lines lw 3  \
,"" using 1:3 title "Müffelklappe" with lines lw 3  \
,"" using 1:(column(4)*100) title "Ventil" with lines lw 3  \
,"" using 1:5 title "HZG Stellwert" with lines lw 3  \

set output "/var/www/html/images/plot_LMV_dwoche.png"
plot "/opt/poopmaster/data/LMV_gp_dwoche.csv" using 1:2 title "Lüfter" with lines lw 3  \
,"" using 1:3 title "Müffelklappe" with lines lw 3  \
,"" using 1:(column(4)*100) title "Ventil" with lines lw 3  \
,"" using 1:5 title "HZG Stellwert" with lines lw 3  \

set output "/var/www/html/images/plot_LMV_lwoche.png"
plot "/opt/poopmaster/data/LMV_gp_lwoche.csv" using 1:2 title "Lüfter" with lines lw 3  \
,"" using 1:3 title "Müffelklappe" with lines \
,"" using 1:(column(4)*100) title "Ventil" with lines \
,"" using 1:5 title "HZG Stellwert" with lines \

set output "/var/www/html/images/plot_LMV_dmonat.png"
plot "/opt/poopmaster/data/LMV_gp_dmonat.csv" using 1:2 title "Lüfter" with lines lw 3  \
,"" using 1:3 title "Müffelklappe" with lines lw 3  \
,"" using 1:(column(4)*100) title "Ventil" with lines lw 3  \
,"" using 1:5 title "HZG Stellwert" with lines lw 3  \

set output "/var/www/html/images/plot_LMV_lmonat.png"
plot "/opt/poopmaster/data/LMV_gp_lmonat.csv" using 1:2 title "Lüfter" with lines lw 3  \
,"" using 1:3 title "Müffelklappe" with lines lw 3  \
,"" using 1:(column(4)*100) title "Ventil" with lines lw 3  \
,"" using 1:5 title "HZG Stellwert" with lines lw 3  \

EOF

VAR1="/opt/poopmaster/data/wochengrafiken/plot_LMV_KW"
VAR2=`date +%V`
VAR3=".png"
VAR4="$VAR1$VAR2$VAR3"
cp /var/www/html/images/plot_LMV_lwoche.png $VAR4

