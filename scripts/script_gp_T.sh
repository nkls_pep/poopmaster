#!/bin/sh
#alle Daten lesen
#mysql -u scheisshaus --password=klo123 -D scheisshaus --skip-column-names -e "SELECT Zeit,T1,T2,T3,T4,T5,T6,T7,T8,T9,T10 FROM daten order by Zeit asc;" | sed 's/\t/;/g;s/\n//g' > /opt/poopmaster/data/T_gp.csv

#letzte Stunde
mysql -h 192.168.1.201 -u scheisshaus --password=klo123 -D scheisshaus --skip-column-names -e \
"SELECT Zeit,T1,T2,T3,T4,T5,T6,T7,T8,T9,T10 FROM daten WHERE Zeit >= DATE_SUB(NOW(),INTERVAL 1 HOUR) order by Zeit asc;" | \
sed 's/\t/;/g;s/\n//g' > /opt/poopmaster/data/T_gp_lstunde.csv

#heute
mysql -h 192.168.1.201 -u scheisshaus --password=klo123 -D scheisshaus --skip-column-names -e \
"SELECT Zeit,T1,T2,T3,T4,T5,T6,T7,T8,T9,T10 FROM daten WHERE DATE(Zeit)=CURDATE() order by Zeit asc;" | \
sed 's/\t/;/g;s/\n//g' > /opt/poopmaster/data/T_gp_heute.csv

#gestern
mysql -h 192.168.1.201 -u scheisshaus --password=klo123 -D scheisshaus --skip-column-names -e \
"SELECT Zeit,T1,T2,T3,T4,T5,T6,T7,T8,T9,T10 FROM daten WHERE DATE(Zeit)=SUBDATE(CURDATE(),1) order by Zeit asc;" | \
sed 's/\t/;/g;s/\n//g' > /opt/poopmaster/data/T_gp_gestern.csv

#diese Woche
mysql -h 192.168.1.201 -u scheisshaus --password=klo123 -D scheisshaus --skip-column-names -e \
"SELECT Zeit,T1,T2,T3,T4,T5,T6,T7,T8,T9,T10 FROM daten WHERE YEARWEEK(Zeit,1)=YEARWEEK(NOW(),1) order by Zeit asc;" | \
sed 's/\t/;/g;s/\n//g' > /opt/poopmaster/data/T_gp_dwoche.csv

#letzte Woche
mysql -h 192.168.1.201 -u scheisshaus --password=klo123 -D scheisshaus --skip-column-names -e \
"SELECT Zeit,T1,T2,T3,T4,T5,T6,T7,T8,T9,T10 FROM daten WHERE YEARWEEK(Zeit,1) = YEARWEEK(NOW() - INTERVAL 1 WEEK,1) order by Zeit asc;" | \
sed 's/\t/;/g;s/\n//g' > /opt/poopmaster/data/T_gp_lwoche.csv

#dieser Monat
mysql -h 192.168.1.201 -u scheisshaus --password=klo123 -D scheisshaus --skip-column-names -e \
"SELECT Zeit,T1,T2,T3,T4,T5,T6,T7,T8,T9,T10 FROM daten WHERE YEAR(Zeit) = YEAR(CURRENT_DATE()) \
AND MONTH(Zeit) = MONTH(CURRENT_DATE()) order by Zeit asc;" | \
sed 's/\t/;/g;s/\n//g' > /opt/poopmaster/data/T_gp_dmonat.csv

#letzter Monat
mysql -h 192.168.1.201 -u scheisshaus --password=klo123 -D scheisshaus --skip-column-names -e \
"SELECT Zeit,T1,T2,T3,T4,T5,T6,T7,T8,T9,T10 FROM daten WHERE YEAR(Zeit) = YEAR(CURRENT_DATE - INTERVAL 1 MONTH) \
AND MONTH(Zeit) = MONTH(CURRENT_DATE - INTERVAL 1 MONTH) order by Zeit asc;" | \
sed 's/\t/;/g;s/\n//g' > /opt/poopmaster/data/T_gp_lmonat.csv


TITLE="T-Sensoren"
/usr/bin/gnuplot << EOF
set datafile separator ';'
set title "$TITLE"
set grid
set xlabel "Zeit"
set autoscale x
set yrange [15:80]
set ylabel "Temperatur (°C)"
set xdata time
set timefmt "%Y-%m-%d %H:%M:%S"
set format x "%d.%m\n%R" 
set key outside right center box title ""
set terminal png large size 1920,1080

set output "/var/www/html/images/plot_T_lstunde.png"
plot "/opt/poopmaster/data/T_gp_lstunde.csv" using 1:2 title "T1 hi ob li" with lines lw 3 \
,"" using 1:3 title "T2 hi ob mi" with lines lw 3 \
,"" using 1:4 title "T3 hi ob re" with lines lw 3 \
,"" using 1:5 title "T4 hi un re" with lines lw 3 \
,"" using 1:6 title "T5 hi un li" with lines lw 3 \
,"" using 1:7 title "T6 un hi" with lines lw 3 \
,"" using 1:8 title "T7 un vo" with lines lw 3 \
,"" using 1:9 title "T8 vo re" with lines lw 3 \
,"" using 1:10 title "T9 vo mi" with lines lw 3 \
,"" using 1:11 title "T10 vo li" with lines lw 3 \

set output "/var/www/html/images/plot_T_heute.png"
plot "/opt/poopmaster/data/T_gp_heute.csv" using 1:2 title "T1 hi ob li" with lines lw 3 \
,"" using 1:3 title "T2 hi ob mi" with lines lw 3 \
,"" using 1:4 title "T3 hi ob re" with lines lw 3 \
,"" using 1:5 title "T4 hi un re" with lines lw 3 \
,"" using 1:6 title "T5 hi un li" with lines lw 3 \
,"" using 1:7 title "T6 un hi" with lines lw 3 \
,"" using 1:8 title "T7 un vo" with lines lw 3 \
,"" using 1:9 title "T8 vo re" with lines lw 3 \
,"" using 1:10 title "T9 vo mi" with lines lw 3 \
,"" using 1:11 title "T10 vo li" with lines lw 3 \

set output "/var/www/html/images/plot_T_gestern.png"
plot "/opt/poopmaster/data/T_gp_gestern.csv"  using 1:2 title "T1 hi ob li" with lines lw 3 \
,"" using 1:3 title "T2 hi ob mi" with lines lw 3 \
,"" using 1:4 title "T3 hi ob re" with lines lw 3 \
,"" using 1:5 title "T4 hi un re" with lines lw 3 \
,"" using 1:6 title "T5 hi un li" with lines lw 3 \
,"" using 1:7 title "T6 un hi" with lines lw 3 \
,"" using 1:8 title "T7 un vo" with lines lw 3 \
,"" using 1:9 title "T8 vo re" with lines lw 3 \
,"" using 1:10 title "T9 vo mi" with lines lw 3 \
,"" using 1:11 title "T10 vo li" with lines lw 3 \

plot "/opt/poopmaster/data/T_gp_dwoche.csv"  using 1:2 title "T1 hi ob li" with lines lw 3 \
,"" using 1:3 title "T2 hi ob mi" with lines lw 3 \
,"" using 1:4 title "T3 hi ob re" with lines lw 3 \
,"" using 1:5 title "T4 hi un re" with lines lw 3 \
,"" using 1:6 title "T5hi un li" with lines lw 3 \
,"" using 1:7 title "T6 un hi" with lines lw 3 \
,"" using 1:8 title "T7 un vo" with lines lw 3 \
,"" using 1:9 title "T8 vo re" with lines lw 3 \
,"" using 1:10 title "T9 vo mi" with lines lw 3 \
,"" using 1:11 title "T10 vo li" with lines lw 3 \

set output "/var/www/html/images/plot_T_lwoche.png"
plot "/opt/poopmaster/data/T_gp_lwoche.csv"  using 1:2 title "T1 hi ob li" with lines lw 3 \
,"" using 1:3 title "T2 hi ob mi" with lines lw 3 \
,"" using 1:4 title "T3 hi ob re" with lines lw 3 \
,"" using 1:5 title "T4 hi un re" with lines lw 3 \
,"" using 1:6 title "T5 hi un li" with lines lw 3 \
,"" using 1:7 title "T6 un hi" with lines lw 3 \
,"" using 1:8 title "T7 un vo" with lines lw 3 \
,"" using 1:9 title "T8 vo re" with lines lw 3 \
,"" using 1:10 title "T9 vo mi" with lines lw 3 \
,"" using 1:11 title "T10 vo li" with lines lw 3 \

set output "/var/www/html/images/plot_T_dmonat.png"
plot "/opt/poopmaster/data/T_gp_dmonat.csv"  using 1:2 title "T1 hi ob li" with lines lw 3 \
,"" using 1:3 title "T2 hi ob mi" with lines lw 3 \
,"" using 1:4 title "T3 hi ob re" with lines lw 3 \
,"" using 1:5 title "T4 hi un re" with lines lw 3 \
,"" using 1:6 title "T5 hi un li" with lines lw 3 \
,"" using 1:7 title "T6 un hi" with lines lw 3 \
,"" using 1:8 title "T7 un vo" with lines lw 3 \
,"" using 1:9 title "T8 vo re" with lines lw 3 \
,"" using 1:10 title "T9 vo mi" with lines lw 3 \
,"" using 1:11 title "T10 vo li" with lines lw 3 \

set output "/var/www/html/images/plot_T_lmonat.png"
plot "/opt/poopmaster/data/T_gp_lmonat.csv"  using 1:2 title "T1 hi ob li" with lines lw 3 \
,"" using 1:3 title "T2 hi ob mi" with lines lw 3 \
,"" using 1:4 title "T3 hi ob re" with lines lw 3 \
,"" using 1:5 title "T4 hi un re" with lines lw 3 \
,"" using 1:6 title "T5 hi un li" with lines lw 3 \
,"" using 1:7 title "T6 un hi" with lines lw 3 \
,"" using 1:8 title "T7 un vo" with lines lw 3 \
,"" using 1:9 title "T8 vo re" with lines lw 3 \
,"" using 1:10 title "T9 vo mi" with lines lw 3 \
,"" using 1:11 title "T10 vo li" with lines lw 3 \

EOF

VAR1="/opt/poopmaster/data/wochengrafiken/plot_T_KW"
VAR2=`date +%V`
VAR3=".png"
VAR4="$VAR1$VAR2$VAR3"
cp /var/www/html/images/plot_T_lwoche.png $VAR4
